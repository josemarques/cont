//https://github.com/T-vK/ESP32-BLE-Keyboard
/*
 * This example turns the ESP32 into a Bluetooth LE gamepad that presses buttons and moves axis
 * 
 * Possible buttons are:
 * BUTTON_1 through to BUTTON_14* 
 * 
 * Possible DPAD/HAT switch position values are: 
 * DPAD_CENTERED, DPAD_UP, DPAD_UP_RIGHT, DPAD_RIGHT, DPAD_DOWN_RIGHT, 
 * DPAD_DOWN, DPAD_DOWN_LEFT, DPAD_LEFT, DPAD_UP_LEFT
 * 
 * bleGamepad.setAxes takes the following signed char parameters: 
 * (Left Thumb X, Left Thumb Y, Right Thumb X, Right Thumb Y, Left Trigger, Right Trigger, Hat switch position);
 */
 
//#include <BleGamepad.h> 
#include "src/ESP32_BLE_Gamepad/BleGamepad.h"
/*#include <BleKeyboard.h>
#include <BleMouse.h>*/


BleGamepad bleGamepad("MARQ","MARQ",100);
//BleKeyboard bleKeyboard;
//BleMouse bleMouse;

int X1=0;
int X2=0;
int Y1=0;
int Y2=0;

int dX1=0;
int dX2=0;
int dY1=0;
int dY2=0;

int NMED=5;


bool DP_UP=false;
bool DP_DN=false;
bool DP_LF=false;
bool DP_RG=false;
int DP_P=0;/*
DPAD_CENTERED  0
DPAD_UP     1
DPAD_UP_RIGHT   2
DPAD_RIGHT    3
DPAD_DOWN_RIGHT 4
DPAD_DOWN     5
DPAD_DOWN_LEFT  6
DPAD_LEFT     7
DPAD_UP_LEFT  8*/
void DP_POS(){
  if(DP_UP==0&&DP_DN==0&&DP_LF==0&&DP_RG==0){
    DP_P=0;
  }
  else if(DP_UP==1&&DP_DN==0&&DP_LF==0&&DP_RG==0){
    DP_P=1;  
  }
  else if(DP_UP==0&&DP_DN==1&&DP_LF==0&&DP_RG==0){
    DP_P=5;  
  }
  else if(DP_UP==0&&DP_DN==0&&DP_LF==1&&DP_RG==0){
    DP_P=7;  
  }
  else if(DP_UP==0&&DP_DN==0&&DP_LF==0&&DP_RG==1){
    DP_P=3;  
  }
  else if(DP_UP==1&&DP_DN==0&&DP_LF==0&&DP_RG==1){
    DP_P=2;  
  }
  else if(DP_UP==1&&DP_DN==0&&DP_LF==1&&DP_RG==0){
    DP_P=8;  
  }
  else if(DP_UP==0&&DP_DN==1&&DP_LF==0&&DP_RG==1){
    DP_P=4;  
  }
  else if(DP_UP==0&&DP_DN==1&&DP_LF==1&&DP_RG==0){
    DP_P=6;  
  }

}

int ANALOG_MOD(int A){
 /* int RES=2048;

  int Aa=0;
  int Ae=0;

  float b=268.5864;
  float e=2.718282;

  
  if(A>=2048){
    Aa=(A-2048);
    RES=pow((float)e,(float)Aa/b)-1;
    RES=RES+2048;
   }
  else{
    Ae=(A);
    RES=pow((float)e,(float)Ae/b)-1;
  }

return RES;*/
return A;  
  }


bool AB=false;
bool BB=false;
bool CB=false;
bool DB=false;

bool LFT=false;
bool RGT=false;

bool AP1=false;
bool AP2=false;

bool dDP_UP=false;
bool dDP_DN=false;
bool dDP_LF=false;
bool dDP_RG=false;

bool dAB=false;
bool dBB=false;
bool dCB=false;
bool dDB=false;

bool dLFT=false;
bool dRGT=false;

bool dAP1=false;
bool dAP2=false;

/*void IRAM_ATTR BTT_PRESS() {
  AB=digitalRead(17);
  BB=digitalRead(16);
  CB=digitalRead(26);
  DB=digitalRead(25);

  LFT=digitalRead(18);
  RGT=digitalRead(27);

  AP1=digitalRead(19);
  AP2=digitalRead(5);
}

void IRAM_ATTR DP_PRESS() {
  DP_UP=digitalRead(3);
  DP_DN=digitalRead(1);
  DP_RG=digitalRead(22);
  DP_LF=digitalRead(21);
  
  DP_POS();
}*/


void setup() {
  //Serial.begin(115200);

  bleGamepad.begin();
  //bleKeyboard.begin();
    //bleMouse.begin();
    pinMode(2,OUTPUT);//On board led

    pinMode(22,INPUT);
    pinMode(23,INPUT);
    pinMode(3,INPUT);
    pinMode(21,INPUT);

    pinMode(17,INPUT);
    pinMode(16,INPUT);
    pinMode(26,INPUT);
    pinMode(25,INPUT);

    pinMode(19,INPUT);
    pinMode(5,INPUT);

    pinMode(18,INPUT);
    pinMode(27,INPUT);

    /*attachInterrupt(digitalPinToInterrupt(22), DP_PRESS, CHANGE);//DP_RG
    attachInterrupt(digitalPinToInterrupt(1), DP_PRESS, CHANGE);//DP_DN
    attachInterrupt(digitalPinToInterrupt(3), DP_PRESS, CHANGE);//DP_UP
    attachInterrupt(digitalPinToInterrupt(21), DP_PRESS, CHANGE);//DP_LF
    
    attachInterrupt(digitalPinToInterrupt(17), BTT_PRESS, CHANGE);//AB
    attachInterrupt(digitalPinToInterrupt(16), BTT_PRESS, CHANGE);//BB
    attachInterrupt(digitalPinToInterrupt(26), BTT_PRESS, CHANGE);//CB
    attachInterrupt(digitalPinToInterrupt(25), BTT_PRESS, CHANGE);//DB

    attachInterrupt(digitalPinToInterrupt(19), BTT_PRESS, CHANGE);//AP1
    attachInterrupt(digitalPinToInterrupt(5), BTT_PRESS, CHANGE);//AP2
    
    attachInterrupt(digitalPinToInterrupt(18), BTT_PRESS, CHANGE);//LFT
    attachInterrupt(digitalPinToInterrupt(27), BTT_PRESS, CHANGE);//RGT
    */
    
}

void loop() {
  if(bleGamepad.isConnected()) {
    digitalWrite(2,HIGH);  
  }
  else{
    digitalWrite(2,LOW);
  }
  
//Leer stics analógicos
analogSetCycles(255);
analogSetSamples(1);

X1=0;
Y1=0;
X2=0;
Y2=0;
for(int i=0;i<NMED;i++){
  X1+=(4095)-analogRead(35);
  Y1+=analogRead(34);
  X2+=(4095)-analogRead(36);
  Y2+=(4095)-analogRead(39);
  delay(2);
  }

X1=X1/NMED;
Y1=Y1/NMED;
X2=X2/NMED;
Y2=Y2/NMED;



X1=ANALOG_MOD(X1);
Y1=ANALOG_MOD(Y1);
X2=ANALOG_MOD(X2);
Y2=ANALOG_MOD(Y2);

//Serial.println("X1");
//Serial.println(X1);
//Serial.println("Y1");
//Serial.println(Y1);
      //Serial.println("X2");
//Serial.println(X2);
      //Serial.println("Y2");
//Serial.println(Y2);

X1=map(X1, 0, 4095, 127, -127);
Y1=map(Y1, 0, 4095, 127, -127);
X2=map(X2, 0, 4095, 127, -127);
Y2=map(Y2, 0, 4095, 127, -127);


//Condicion de movimiento de sticks analógicos
/*int play=0;
bool cond1=(dX1-X1)>play||(dX1-X1)<-play;
bool cond2=(dY1-Y1)>play||(dY1-Y1)<-play;
bool cond3=(dX2-X2)>play||(dX2-X2)<-play;
bool cond4=(dY2-Y2)>play||(dY2-Y2)<-play;*/

/* DP_UP=!digitalRead(3);
  DP_DN=!digitalRead(23);
  DP_RG=!digitalRead(22);
  DP_LF=!digitalRead(21);
  DP_POS();
   */


//Condición de movimiento de DPAD
/*bool DPC_1=dDP_UP!=DP_UP;
bool DPC_2=dDP_DN!=DP_DN;
bool DPC_3=dDP_LF!=DP_LF;
bool DPC_4=dDP_RG!=DP_RG;*/

    //if(cond1||cond2||cond3||cond4/*||DPC_1||DPC_2||DPC_3||DPC_4*/){//Si los joistics se mueven se actualiza la pos

   /*   Serial.println("X1");
Serial.println(X1);
Serial.println("Y1");
Serial.println(Y1);
      Serial.println("X2");
Serial.println(X2);
      Serial.println("Y2");
Serial.println(Y2);*/

//Serial.println("DPAD");
//Serial.println(DP_P);
if(bleGamepad.isConnected()) {
      bleGamepad.setAxes(X2,Y2,X1,Y1,0,0,0 /*DP_P*/);
//}


  //Raton
  
   /*if(bleMouse.isConnected()) {
    //Serial.println("Scroll Down");
    bleMouse.move(X1/16,Y1/16,Y2/16,X2/16);
  }*/
  }

    //Manejo de diferenciales de posicion
    dX1=X1;
    dY1=Y1;
    dX2=X2;
    dY2=Y2;

   /* dDP_UP=DP_UP;
    dDP_DN=DP_DN;
    dDP_LF=DP_LF;
    dDP_RG=DP_RG;
*/
    //Manejo de diferenciales de posicion

    

  //Lectura y control de botones digitales.
 
  AB=!digitalRead(17);
  BB=!digitalRead(16);
  CB=!digitalRead(26);
  DB=!digitalRead(25);

  LFT=!digitalRead(18);
  RGT=!digitalRead(27);

  AP1=!digitalRead(19);
  AP2=!digitalRead(5);

  //Lectura del DPAD
  DP_UP=!digitalRead(3);
  DP_DN=!digitalRead(23);
  DP_RG=!digitalRead(22);
  DP_LF=!digitalRead(21);

//Manejo de pulsación de botones
//Condición de cambio de botones

bool BT_1=dAB!=AB;
bool BT_2=dBB!=BB;
bool BT_3=dCB!=CB;
bool BT_4=dDB!=DB;

bool BT_5=dLFT!=LFT;
bool BT_6=dRGT!=RGT;

bool BT_7=dAP1!=AP1;
bool BT_8=dAP2!=AP2;

bool DPC_1=dDP_UP!=DP_UP;
bool DPC_2=dDP_DN!=DP_DN;
bool DPC_3=dDP_LF!=DP_LF;
bool DPC_4=dDP_RG!=DP_RG;

  if(bleGamepad.isConnected()){
    if(BT_1){
      if(AB){
        bleGamepad.press(BUTTON_1);
        //Serial.println("BT_1_P");
        }
      else {
        bleGamepad.release(BUTTON_1);}
     //Serial.println("BT_1_R");
    }
    if(BT_2){
      if(BB){
        bleGamepad.press(BUTTON_2);
        //Serial.println("BT_2_P");
        }
      else {
        bleGamepad.release(BUTTON_2);}
      //Serial.println("BT_2_R");
    }
    if(BT_3){
      if(CB){
        bleGamepad.press(BUTTON_3);
        //Serial.println("BT_3_P");
        }
      else {
        bleGamepad.release(BUTTON_3);}
      //Serial.println("BT_3_R");
    }
    if(BT_4){
      if(DB){
        bleGamepad.press(BUTTON_4);
        //Serial.println("BT_4_P");
        }
      else {
        bleGamepad.release(BUTTON_4);}
      //Serial.println("BT_4_R");
    }
    if(BT_5){
      if(LFT){
        bleGamepad.press(BUTTON_5);
        //Serial.println("BT_5_P");
        }
      else {
        bleGamepad.release(BUTTON_5);}
      //Serial.println("BT_5_R");
    }
    if(BT_6){
      if(RGT){
        bleGamepad.press(BUTTON_6);
        //Serial.println("BT_6_P");
        }
      else {
        bleGamepad.release(BUTTON_6);}
     // Serial.println("BT_6_R");
    }
    if(BT_7){
      if(AP1){
        bleGamepad.press(BUTTON_7);
        //Serial.println("BT_7_P");
        }
      else {
        bleGamepad.release(BUTTON_7);}
      //Serial.println("BT_7_R");
    }
    if(BT_8){
      if(AP2){
        bleGamepad.press(BUTTON_8);
        //Serial.println("BT_8_P");
        }
      else {
        bleGamepad.release(BUTTON_8);}
      //Serial.println("BT_8_R");
    }

    if(DPC_1){
      if(DP_UP){
        bleGamepad.press(BUTTON_9);
        //Serial.println("BT_9_P");
        }
      else {
        bleGamepad.release(BUTTON_9);}
      //Serial.println("BT_9_R");
    }

    if(DPC_2){
      if(DP_DN){
        bleGamepad.press(BUTTON_10);
        //Serial.println("BT_10_P");
        }
      else {
        bleGamepad.release(BUTTON_10);}
      //Serial.println("BT_10_R");
    }

    if(DPC_3){
      if(DP_LF){
        bleGamepad.press(BUTTON_11);
        //Serial.println("BT_11_P");
        }
      else {
        bleGamepad.release(BUTTON_11);}
      //Serial.println("BT_8_R");
    }

    if(DPC_4){
      if(DP_RG){
        bleGamepad.press(BUTTON_12);
        //Serial.println("BT_12_P");
        }
      else {
        bleGamepad.release(BUTTON_12);}
      //Serial.println("BT_12_R");
    }
  }


dDP_UP=DP_UP;
dDP_DN=DP_DN;
dDP_LF=DP_LF;
dDP_RG=DP_RG;

dAB=AB;
dBB=BB;
dCB=CB;
dDB=DB;

dLFT=LFT;
dRGT=RGT;

dAP1=AP1;
dAP2=AP2;
  
    
    //Serial.println("Press buttons 1 and 14. Move all axes to max. Set DPAD to down right.");
    //bleGamepad.press(BUTTON_14);
    //bleGamepad.press(BUTTON_1);
    //bleGamepad.setAxes(127, 127, 127, 127, 127, 127, DPAD_DOWN_RIGHT);
    //delay(500);

    //Serial.println("Release button 14. Move all axes to min. Set DPAD to centred.");
    //bleGamepad.release(BUTTON_14);
    //bleGamepad.setAxes(-127, -127, -127, -127, -127, -127, DPAD_CENTERED);
    //delay(500);
  

  //Teclado
/*
if(bleKeyboard.isConnected()) {
    Serial.println("Sending 'Hello world'...");
    bleKeyboard.print("Hello world");

    delay(1000);

    Serial.println("Sending Enter key...");
    bleKeyboard.write(KEY_RETURN);

    delay(1000);

    Serial.println("Sending Play/Pause media key...");
    bleKeyboard.write(KEY_MEDIA_PLAY_PAUSE);

    delay(1000);

    Serial.println("Sending Ctrl+Alt+Delete...");
    bleKeyboard.press(KEY_LEFT_CTRL);
    bleKeyboard.press(KEY_LEFT_ALT);
    bleKeyboard.press(KEY_DELETE);
    delay(100);
    bleKeyboard.releaseAll();

  }
  Serial.println("Waiting 5 seconds...");
  delay(5000);*/

  
}
